
import java.util.Stack;

public class Tnode {

    private String name;
    private Tnode firstChild;
    private Tnode nextSibling;

    //Constructor for TNode
    Tnode(String n, Tnode f, Tnode s) {
        name = n;
        firstChild = f;
        nextSibling = s;
    }

    //See ei meeldi mulle, sest see töötab ainult kahendpuuga
    @Override
    public String toString() {
        StringBuffer b = new StringBuffer();
        b.append(name);
        if (firstChild == null) {
            return b.toString();
        }
        b.append("(");
        b.append(firstChild);
        b.append(",");
        b.append(nextSibling);
        b.append(")");
        return b.toString();
    }

    //Variant, mida ma tööle ei saanud. Kas b.append(holder); kutsub äkki välja toString meetodi, mis on mul üle
    //kirjutatud, aga sinna oleks tavalist vaja?

    /* public String toString() {
        StringBuffer b = new StringBuffer();
        b.append(name);
        Tnode holder = firstChild;
        boolean parenthesis = holder != null;
        //Kui on lapsi, alusta suluga
        if (parenthesis)
            b.append("(");
        //Kui samal tasemel veel vendi, siis eralda komaga)
        while (holder != null) {
            b.append(holder);
            holder = holder.nextSibling;
            if (holder != null)
                b.append(",");
        }

        //Lõpeta sulg ära ka, kui lapsi oli
        if (parenthesis)
            b.append(")");
        return b.toString();
    }*/
    //Based on https://stackoverflow.com/questions/19895308/converting-from-reverse-polish-notationrpn-into-a-tree-form
    public static Tnode buildFromRPN(String pol) {
        if (pol.isEmpty()) {
            throw new RuntimeException("Empty input string: " + pol);
        }
        int symbols = 0;
        Stack<Tnode> stack = new Stack<Tnode>();
        String[] parts = pol.split("\\s");
        int total = parts.length;
        for (String c : parts) {
            if (!((c.matches("[/*]?[+-]?([0-9]*[.])?[0-9]*")) || c.equals("SWAP") || c.equals("ROT"))) {
                throw new RuntimeException("Unknown symbol '" + c + "' in input string '" + pol + "'");
            }
            if(c.equals("SWAP") || c.equals("ROT")) {
                if (c.equals("SWAP")) {
                    if (
                            stack.size() < 2) {
                        throw new RuntimeException("Not enough numeric arguments for '" + c + "' in expression  '" + pol + "'");
                    }
                    Tnode first = stack.pop();
                    Tnode second = stack.pop();
                    stack.push(first);
                    stack.push(second);
                }
                if (c.equals("ROT")) {
                    if (
                            stack.size() < 3) {
                        throw new RuntimeException("Not enough numeric arguments for '" + c + "' in expression  '" + pol + "'");
                    }
                    total--;
                    Tnode secondrot = stack.pop();
                    Tnode firstrot = stack.pop();
                    Tnode thirdrot = stack.pop();
                    stack.push(firstrot);
                    stack.push(secondrot);
                    stack.push(thirdrot);
                }
            }
            else {
                Tnode root = new Tnode(c, null, null);
                if ("+-*/".indexOf(c) != -1) {
                    symbols++;
                    root.nextSibling = stack.pop();
                    root.firstChild = stack.pop();
                }
                stack.push(root);
            }
        }
        if (symbols >= total - symbols) {
            System.out.println(total);
            throw new RuntimeException("There are not enough arguments in string '" + pol + "'");
        } else if (stack.size() > 1) {
            throw new RuntimeException("There are too many arguments in string '" + pol + "'");
        }
        return stack.pop();
    }


    public static void main(String[] param) {
        String rpn = "1 2 +";
        System.out.println("RPN: " + rpn);
        Tnode res = buildFromRPN(rpn);
        System.out.println("Tree: " + res);
        String s = "2 5 SWAP -"; //-(5,2)
        Tnode t = Tnode.buildFromRPN(s);
        System.out.println("Tree: " + t);
        String k = "2 5 9 ROT - +"; //+(5,-(9,2))
        Tnode l = Tnode.buildFromRPN(k);
        System.out.println("Tree: " + l);
        String u = "2 5 9 ROT + SWAP -"; // -(+(9,2),5)
        Tnode v = Tnode.buildFromRPN(u);
        System.out.println("Tree: " + v);
        // TODO!!! Your tests here
    }
}

